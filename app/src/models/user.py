from flask import jsonify

from app import db
from flask_login import UserMixin

from src.models.mixins import DbMixin


class User(DbMixin, UserMixin, db.Model):
    """
    Simple user model
    """

    __tablename__ = "users"

    # primary keys
    id = db.Column(db.Integer, primary_key=True)

    # model description
    username = db.Column(db.String(32), index=True)
    email = db.Column(db.String(32), )
    password_hash = db.Column(db.String(128))

    def __repr__(self) -> str:
        return f"User<id={self.id}, username={self.username}>"

    def __str__(self) -> str:
        return self.username

    def serialize(self) -> dict:
        return {
            "id": self.id,
            "username": self.username,
            "email": self.email
        }
