class Tender(object):
    link: str = ""
    number: str = ""
    name: str = ""
    price: str = ""
    int_price: int = 0

    def __str__(self) -> str:
        return f"""
                Tender:
                link: {self.link},
                number: {self.number},
                name: {self.name}
                price: {self.price}
                """

    def __repr__(self) -> str:
        return f"""
                Tender:
                link: {self.link},
                number: {self.number},
                name: {self.name}
                price: {self.price}
                """

    def cast_tender_price_to_int(self):
        import re
        self.int_price = int(re.sub(r"[^0-9]", "", self.price))
        self.int_price //= 100


class Customer(object):
    link: str = ""
    name: str = ""
    phone_number: str = ""
    inn: str = ""
    kpp: str = ""
    orgn: str = ""
    mail: str = ""
    site: str = ""

    def __str__(self) -> str:
        return f"""
                Customer:
                link: {self.link},
                name: {self.name},
                phone_number: {self.phone_number},
                inn: {self.inn}
                kpp: {self.kpp}
                orgn: {self.orgn}
                mail: {self.mail}
                site: {self.site}
                """

    def __repr__(self):
        return f"""
                        Customer:
                        link: {self.link},
                        name: {self.name},
                        phone_number: {self.phone_number},
                        inn: {self.inn}
                        kpp: {self.kpp}
                        orgn: {self.orgn}
                        mail: {self.mail}
                        site: {self.site}
                        """


class Supplier(Customer):
    price: str = ""
    int_price: int = 0
    count: int = 0
    tax: str = ""
    offer_descr: str = ""

    def cast_tender_price_to_int(self):
        import re
        self.int_price = int(re.sub(r"[^0-9]", "", self.price))
        self.int_price //= 100

    def __str__(self) -> str:
        return f"""
                   Supplier:
                   link: {self.link},
                   name: {self.name},
                   phone_number: {self.phone_number},
                   inn: {self.inn}
                   kpp: {self.kpp}
                   orgn: {self.orgn}
                   mail: {self.mail}
                   site: {self.site}
                   offer_descr: {self.offer_descr}
                   """

    def __repr__(self):
        return f"""
                   Supplier:
                   link: {self.link},
                   name: {self.name},
                   inn: {self.inn}
                   price: {self.price}
                   int_price: {self.int_price}
                   offer_descr: {self.offer_descr}
                   """
