from flask_migrate import Migrate, upgrade
from src import create_app, db

app = create_app()
migrate = Migrate(app, db)

from src.models import user
